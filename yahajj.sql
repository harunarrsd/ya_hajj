-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2019 at 05:35 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yahajj`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `aktifitas`
--

CREATE TABLE `aktifitas` (
  `id` int(11) NOT NULL,
  `idpaket` int(11) NOT NULL,
  `hari` varchar(12) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `deskripsi` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aktifitas`
--

INSERT INTO `aktifitas` (`id`, `idpaket`, `hari`, `judul`, `deskripsi`) VALUES
(1, 1, 'Hari 1 :', 'Jalan - Jalan Ke Madinah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'),
(2, 1, 'Hari 2 :', 'Pergi Belanja', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.');

-- --------------------------------------------------------

--
-- Table structure for table `bursa`
--

CREATE TABLE `bursa` (
  `id` int(11) NOT NULL,
  `idpaket` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE `favorite` (
  `id` int(11) NOT NULL,
  `idpaket` int(11) NOT NULL,
  `iduser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorite`
--

INSERT INTO `favorite` (`id`, `idpaket`, `iduser`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE `paket` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `deskripsi` varchar(500) DEFAULT NULL,
  `pj` varchar(25) DEFAULT NULL,
  `jumlah_hari` varchar(25) NOT NULL,
  `harga` int(12) NOT NULL,
  `id_travel` int(11) NOT NULL,
  `harga_termasuk` varchar(100) DEFAULT NULL,
  `tidak_termasuk` varchar(100) DEFAULT NULL,
  `pembatalan` varchar(100) DEFAULT NULL,
  `perlengkapan` varchar(100) DEFAULT NULL,
  `jenis` varchar(25) NOT NULL,
  `berangkat` varchar(50) DEFAULT NULL,
  `tiba` varchar(50) DEFAULT NULL,
  `gambar` varchar(150) DEFAULT NULL,
  `status` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id`, `nama`, `deskripsi`, `pj`, `jumlah_hari`, `harga`, `id_travel`, `harga_termasuk`, `tidak_termasuk`, `pembatalan`, `perlengkapan`, `jenis`, `berangkat`, `tiba`, `gambar`, `status`) VALUES
(1, 'Umroh Travel Mekah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', 'Usman', '13 Hari 12 Malam', 25000000, 1, 'Ongkir', 'Pengiriman', 'Sukses', 'Mantap', 'Umroh Regular', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 2),
(2, 'Paket Umroh Plus Plus', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', 25000000, 1, NULL, NULL, NULL, NULL, 'Umroh Regular', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 1),
(3, 'Paket Umroh Plus Plus', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', 25000000, 1, NULL, NULL, NULL, NULL, 'Umroh Regular', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 1),
(4, 'Paket Umroh Plus Plus', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', 25000000, 1, NULL, NULL, NULL, NULL, 'Umroh Regular', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 3),
(5, 'Umroh Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Umroh Plus', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 4),
(6, 'Umroh Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Umroh Plus', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 0),
(7, 'Umroh Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Umroh Plus', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 0),
(8, 'Haji Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Haji', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 0),
(9, 'Haji Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Haji', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 0),
(10, 'Haji Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Haji', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `idtravel` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `bintang` int(11) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_paket`
--

CREATE TABLE `status_paket` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_paket`
--

INSERT INTO `status_paket` (`id`, `nama`) VALUES
(1, 'Sudah Bayar'),
(2, 'Berhasil Berangkat'),
(3, 'Jadwal Dialihkan'),
(4, 'Gagal Berangkat');

-- --------------------------------------------------------

--
-- Table structure for table `status_transaksi`
--

CREATE TABLE `status_transaksi` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_transaksi`
--

INSERT INTO `status_transaksi` (`id`, `nama`) VALUES
(1, 'Belum Bayar'),
(2, 'Dalam Proses Konfirmasi'),
(3, 'Sudah Konfirmasi'),
(4, 'Sudah Berhasil');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `kode_booking` varchar(12) NOT NULL,
  `idpaket` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `total_biaya` int(11) DEFAULT NULL,
  `foto_bukti` varchar(100) DEFAULT NULL,
  `status` int(2) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `kode_booking`, `idpaket`, `iduser`, `total_biaya`, `foto_bukti`, `status`, `tanggal`) VALUES
(1, 'BK.0519.001', 1, 1, 250000000, NULL, 1, '2019-05-01'),
(86, '', 2, 1, NULL, NULL, 1, '0000-00-00'),
(87, '', 6, 1, NULL, NULL, 1, '0000-00-00'),
(88, '', 7, 1, NULL, NULL, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_orang`
--

CREATE TABLE `transaksi_orang` (
  `id` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `jenkel` varchar(25) DEFAULT NULL,
  `nohp` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_orang`
--

INSERT INTO `transaksi_orang` (`id`, `id_transaksi`, `nama`, `jenkel`, `nohp`) VALUES
(1, 1, 'jujun', 'L', '0856323'),
(26, 86, 'masqqq', 'L', '31444'),
(27, 86, 'mamas', 'L', '0855'),
(28, 87, 'test', 'L', '0855'),
(29, 87, 'tassss', 'L', '4555'),
(30, 88, 'sau', 'L', '34144'),
(31, 88, 'test', 'L', '0855');

-- --------------------------------------------------------

--
-- Table structure for table `travel`
--

CREATE TABLE `travel` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_izin` varchar(25) NOT NULL,
  `nohp` varchar(13) NOT NULL,
  `email` varchar(25) DEFAULT NULL,
  `no_rekening` int(25) NOT NULL,
  `deskripsi` varchar(300) DEFAULT NULL,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(25) DEFAULT NULL,
  `foto_izin` varchar(50) DEFAULT NULL,
  `foto_logo` varchar(50) DEFAULT NULL,
  `rating` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `travel`
--

INSERT INTO `travel` (`id`, `nama`, `alamat`, `no_izin`, `nohp`, `email`, `no_rekening`, `deskripsi`, `username`, `password`, `foto_izin`, `foto_logo`, `rating`) VALUES
(1, 'Travel Barokah', 'Kota Depok', '123456', '08587342', 'travelbarokah@gmail.com', 1234567, 'Barokah Pisan Euy', 'barokahtravel', 'barokahtravel', NULL, 'http://thegorbalsla.com/wp-content/uploads/2018/08', 5);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `jenkel` char(25) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `no_telp` varchar(13) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `tgl_dibuat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `alamat`, `jenkel`, `email`, `no_telp`, `username`, `password`, `tgl_dibuat`) VALUES
(1, 'Angga', 'Jl Widyakrama', 'P', 'angga.riansah@gmail.com', '07565', 'angga', 'angga', '2019-05-11 10:33:46'),
(2, 'Test', 'test', 'L', 'a', '45', 'as', 'ss', '2019-05-11 11:36:51'),
(3, 'test', NULL, NULL, 'test@gmail.com', '', 'test', 'test', '2019-05-13 08:14:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpaket` (`idpaket`);

--
-- Indexes for table `bursa`
--
ALTER TABLE `bursa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpaket` (`idpaket`);

--
-- Indexes for table `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpaket` (`idpaket`),
  ADD KEY `iduser` (`iduser`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_travel` (`id_travel`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idtravel` (`idtravel`),
  ADD KEY `iduser` (`iduser`);

--
-- Indexes for table `status_paket`
--
ALTER TABLE `status_paket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_transaksi`
--
ALTER TABLE `status_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpaket` (`idpaket`),
  ADD KEY `iduser` (`iduser`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `transaksi_orang`
--
ALTER TABLE `transaksi_orang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_transaksi` (`id_transaksi`);

--
-- Indexes for table `travel`
--
ALTER TABLE `travel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aktifitas`
--
ALTER TABLE `aktifitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bursa`
--
ALTER TABLE `bursa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_paket`
--
ALTER TABLE `status_paket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `status_transaksi`
--
ALTER TABLE `status_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `transaksi_orang`
--
ALTER TABLE `transaksi_orang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `travel`
--
ALTER TABLE `travel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD CONSTRAINT `aktifitas_ibfk_1` FOREIGN KEY (`idpaket`) REFERENCES `paket` (`id`);

--
-- Constraints for table `bursa`
--
ALTER TABLE `bursa`
  ADD CONSTRAINT `bursa_ibfk_1` FOREIGN KEY (`idpaket`) REFERENCES `paket` (`id`);

--
-- Constraints for table `favorite`
--
ALTER TABLE `favorite`
  ADD CONSTRAINT `favorite_ibfk_1` FOREIGN KEY (`idpaket`) REFERENCES `paket` (`id`),
  ADD CONSTRAINT `favorite_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`);

--
-- Constraints for table `paket`
--
ALTER TABLE `paket`
  ADD CONSTRAINT `paket_ibfk_1` FOREIGN KEY (`id_travel`) REFERENCES `travel` (`id`);

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`idtravel`) REFERENCES `travel` (`id`),
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`idpaket`) REFERENCES `paket` (`id`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`status`) REFERENCES `status_transaksi` (`id`);

--
-- Constraints for table `transaksi_orang`
--
ALTER TABLE `transaksi_orang`
  ADD CONSTRAINT `transaksi_orang_ibfk_1` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
