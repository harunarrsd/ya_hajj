-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 18, 2019 at 09:06 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aflowzco_yahajj`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama`, `username`, `password`) VALUES
(1, 'Test', 'test', '098f6bcd4621d373cade4e832627b4f6'),
(2, 'angga', 'angga', '8479c86c7afcb56631104f5ce5d6de62');

-- --------------------------------------------------------

--
-- Table structure for table `aktifitas`
--

CREATE TABLE `aktifitas` (
  `id` int(11) NOT NULL,
  `idpaket` int(11) NOT NULL,
  `hari` varchar(12) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `deskripsi` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aktifitas`
--

INSERT INTO `aktifitas` (`id`, `idpaket`, `hari`, `judul`, `deskripsi`) VALUES
(1, 1, 'Hari 1 :', 'Jalan - Jalan Ke Madinah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'),
(2, 1, 'Hari 2 :', 'Pergi Belanja', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'),
(4, 2, 'Hari 1', 'Membuat Kaligrafi', 'Lorem ipsum dolor sit amet'),
(5, 2, 'Hari 2', 'Tidur Siang', 'Bobo');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `gambar` varchar(200) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `judul`, `gambar`, `status`) VALUES
(1, 'Discount Cuy', 'https://www.rabbanitour.co.id/wp-content/uploads/2018/12/banner-umroh-reguler-min.png', 'Aktif'),
(2, 'Discount Cuy', 'https://www.rabbanitour.co.id/wp-content/uploads/2018/12/banner-umroh-reguler-min.png', 'Aktif'),
(3, 'Apa we', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 'Tidak Aktif'),
(4, 'test', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 'Aktif'),
(5, 'Berhadiah ', '2019-06-12_04:58:34_5.jpg', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `deskripsi` varchar(2000) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `idadmin` int(11) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `judul`, `deskripsi`, `gambar`, `idadmin`, `tanggal`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetuer ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 1, '2019-05-28 04:00:00'),
(2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 1, '2019-05-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `bursa`
--

CREATE TABLE `bursa` (
  `id` int(11) NOT NULL,
  `idpaket` int(11) DEFAULT NULL,
  `pengambil` int(11) DEFAULT NULL,
  `harga_jamaah` int(25) NOT NULL,
  `status` int(11) NOT NULL,
  `total_bayar` int(25) DEFAULT NULL,
  `foto_bukti` varchar(250) DEFAULT NULL,
  `tgl_ambil` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bursa`
--

INSERT INTO `bursa` (`id`, `idpaket`, `pengambil`, `harga_jamaah`, `status`, `total_bayar`, `foto_bukti`, `tgl_ambil`) VALUES
(1, 1, 1, 200000, 2, NULL, '5d085e6673463.png', '2019-06-18 10:45:42'),
(2, 2, 1, 200000, 1, NULL, NULL, '2019-06-16 01:33:18'),
(8, 3, NULL, 500000, 1, NULL, NULL, '2019-06-16 02:04:55'),
(9, 10, NULL, 200000, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `iduser` int(11) DEFAULT NULL,
  `deskripsi` varchar(750) DEFAULT NULL,
  `idadmin` int(11) DEFAULT NULL,
  `role` varchar(25) DEFAULT NULL,
  `waktu` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `iduser`, `deskripsi`, `idadmin`, `role`, `waktu`) VALUES
(1, 1, 'Assalamualaikum', NULL, 'User', '2019-06-16 04:00:00'),
(2, 1, 'Iyaa ada yang bisa saya bantu', 1, 'Admin', '2019-06-16 04:00:00'),
(3, 3, 'Halo', NULL, 'User', '2019-06-16 11:58:33'),
(4, 3, 'Admin', NULL, 'User', '2019-06-16 11:58:43'),
(5, 3, 'apa kabar', NULL, 'User', '2019-06-16 11:58:48'),
(6, 3, 'Assalamualaikum saya angga riansah ingin menanyakan terkait paket saya yang belum sampai juga', NULL, 'User', '2019-06-16 12:03:50'),
(7, 3, 'Tunggu Sebentar yaah', 1, 'Admin', '2019-06-15 18:00:00'),
(8, 3, 'Baik terimakasih ðŸ˜', NULL, 'User', '2019-06-16 12:08:50'),
(9, 3, 'Mantaaf', NULL, 'User', '2019-06-16 12:17:01'),
(10, 3, 'Halo', NULL, 'User', '2019-06-17 04:36:16'),
(11, 3, 'test again', NULL, 'User', '2019-06-17 13:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE `favorite` (
  `id` int(11) NOT NULL,
  `idpaket` int(11) NOT NULL,
  `iduser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorite`
--

INSERT INTO `favorite` (`id`, `idpaket`, `iduser`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(14, 7, 3),
(15, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fcm_info`
--

CREATE TABLE `fcm_info` (
  `id` int(11) NOT NULL,
  `token` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fcm_info`
--

INSERT INTO `fcm_info` (`id`, `token`) VALUES
(1, ''),
(2, ''),
(3, ''),
(4, ''),
(5, ''),
(6, '');

-- --------------------------------------------------------

--
-- Table structure for table `foto_paket`
--

CREATE TABLE `foto_paket` (
  `id` int(11) NOT NULL,
  `idpaket` int(11) DEFAULT NULL,
  `gambar` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job_apply`
--

CREATE TABLE `job_apply` (
  `id` int(11) NOT NULL,
  `id_request` int(11) DEFAULT NULL,
  `id_muthawif` int(11) DEFAULT NULL,
  `status_job` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_apply`
--

INSERT INTO `job_apply` (`id`, `id_request`, `id_muthawif`, `status_job`) VALUES
(1, 1, 1, 'Ditolak'),
(2, 1, 1, 'Diterima');

-- --------------------------------------------------------

--
-- Table structure for table `muthawif`
--

CREATE TABLE `muthawif` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `username` varchar(25) NOT NULL,
  `no_ktp` int(11) DEFAULT NULL,
  `no_paspor` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `no_telp` varchar(15) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `gambar` varchar(200) DEFAULT NULL,
  `password` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muthawif`
--

INSERT INTO `muthawif` (`id`, `nama`, `username`, `no_ktp`, `no_paspor`, `email`, `no_telp`, `tgl_lahir`, `alamat`, `gambar`, `password`) VALUES
(1, 'test', 'test', 12345678, '54345645', 'test@gmail.com', '0756534', '2019-06-09', 'JL Cipanengah', NULL, 'test'),
(2, 'Alvin', 'alvin', 64646, 'uaja', 'alvin@gmail.com', '64646', '2000-04-08', 'js', NULL, 'qqqqq');

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE `paket` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `deskripsi` varchar(500) DEFAULT NULL,
  `pj` varchar(25) DEFAULT NULL,
  `jumlah_hari` varchar(25) NOT NULL,
  `tgl_berangkat` date DEFAULT NULL,
  `tgl_pulang` date DEFAULT NULL,
  `harga` int(12) NOT NULL,
  `id_travel` int(11) NOT NULL,
  `harga_termasuk` varchar(100) DEFAULT NULL,
  `tidak_termasuk` varchar(100) DEFAULT NULL,
  `pembatalan` varchar(100) DEFAULT NULL,
  `perlengkapan` varchar(100) DEFAULT NULL,
  `jenis` varchar(25) NOT NULL,
  `berangkat` varchar(50) DEFAULT NULL,
  `tiba` varchar(50) DEFAULT NULL,
  `gambar` varchar(150) DEFAULT NULL,
  `status` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id`, `nama`, `deskripsi`, `pj`, `jumlah_hari`, `tgl_berangkat`, `tgl_pulang`, `harga`, `id_travel`, `harga_termasuk`, `tidak_termasuk`, `pembatalan`, `perlengkapan`, `jenis`, `berangkat`, `tiba`, `gambar`, `status`) VALUES
(1, 'Umroh Travel Mekah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', 'Usman', '13 Hari 12 Malam', NULL, NULL, 23000000, 2, 'Ongkir', 'Pengiriman', 'Sukses', 'Mantap', 'Umroh Regular', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 2),
(2, 'Paket Umroh Plus Plus', ' ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '15 Hari 12 Malam', NULL, NULL, 22000000, 1, 'Apa', 'Bekal Pribadi', NULL, 'Makanan Ringan', 'Umroh Regular', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 1),
(3, 'Paket Umroh Plus Plus', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '14 Hari 12 Malam', NULL, NULL, 25000000, 1, NULL, NULL, NULL, NULL, 'Umroh Regular', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 1),
(4, 'Paket Umroh Plus Plus', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', NULL, NULL, 25000000, 1, NULL, NULL, NULL, NULL, 'Umroh Regular', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 3),
(5, 'Umroh Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', NULL, NULL, 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Umroh Plus', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 4),
(6, 'Umroh Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', NULL, NULL, 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Umroh Plus', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 0),
(7, 'Umroh Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', NULL, NULL, 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Umroh Plus', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 0),
(8, 'Haji Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', NULL, NULL, 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Haji', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 0),
(9, 'Haji Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', NULL, NULL, 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Haji', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 0),
(10, 'Haji Dengan Amanah', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu', NULL, '13 Hari 12 Malam', NULL, NULL, 25000000, 1, 'Makan', 'Tidur', 'Tidak Ada', 'Tidak Ada', 'Haji', 'Jakarta', 'Mekkah', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', 0),
(12, 'Haji Mabrur', 'Pastii mabrur pisan', NULL, '12 hari 11 malam', '2019-06-11', '2019-06-11', 21000000, 1, '- Makan\n- Tiket Pesawat\n- Hotel', '- Ongkos ke bandara', NULL, NULL, 'Haji', NULL, NULL, NULL, NULL),
(13, 'Test', 'Ballaalaa lorem ipsum ', NULL, '12 Hari 13 Malam ', '2019-06-16', '2019-06-16', 20000000, 1, '- Tiket Pesawat', 'Tidur', NULL, NULL, 'Umroh Plus', NULL, NULL, '5d05f6fb27c9a.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `request_muthawif`
--

CREATE TABLE `request_muthawif` (
  `id` int(11) NOT NULL,
  `idpaket` int(11) DEFAULT NULL,
  `harga_job` int(11) DEFAULT NULL,
  `pria` int(11) DEFAULT '0',
  `wanita` int(11) DEFAULT '0',
  `total_bayar` int(25) DEFAULT NULL,
  `status_bayar` int(11) DEFAULT NULL,
  `foto_bukti` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_muthawif`
--

INSERT INTO `request_muthawif` (`id`, `idpaket`, `harga_job`, `pria`, `wanita`, `total_bayar`, `status_bayar`, `foto_bukti`) VALUES
(1, 1, 700000, 0, 1, NULL, 1, NULL),
(2, 2, 120000, 1, 0, NULL, 1, NULL),
(6, 10, 100000, 1, 0, NULL, 1, NULL),
(9, 7, 100000, 3, 3, NULL, 1, NULL),
(13, 5, 100000, 0, 2, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `idtravel` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `idtravel`, `iduser`, `rating`, `tanggal`, `keterangan`) VALUES
(1, 1, 1, 5, '2019-05-01', 'Good Joob!!'),
(2, 1, 1, 4, '2019-05-01', 'Good Joob!!'),
(3, 1, 1, 3, '2019-05-01', 'Good Joob!!');

-- --------------------------------------------------------

--
-- Table structure for table `status_bayar`
--

CREATE TABLE `status_bayar` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_bayar`
--

INSERT INTO `status_bayar` (`id`, `nama`) VALUES
(1, 'Belum Bayar'),
(2, 'Sudah Konfirmasi'),
(3, 'Sudah Berhasil');

-- --------------------------------------------------------

--
-- Table structure for table `status_paket`
--

CREATE TABLE `status_paket` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_paket`
--

INSERT INTO `status_paket` (`id`, `nama`) VALUES
(1, 'Sudah Bayar'),
(2, 'Berhasil Berangkat'),
(3, 'Jadwal Dialihkan'),
(4, 'Gagal Berangkat');

-- --------------------------------------------------------

--
-- Table structure for table `status_transaksi`
--

CREATE TABLE `status_transaksi` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_transaksi`
--

INSERT INTO `status_transaksi` (`id`, `nama`) VALUES
(1, 'Belum Bayar'),
(2, 'Dalam Proses Konfirmasi'),
(3, 'Sudah Konfirmasi'),
(4, 'Sudah Berhasil');

-- --------------------------------------------------------

--
-- Table structure for table `tawar_bursa`
--

CREATE TABLE `tawar_bursa` (
  `id` int(11) NOT NULL,
  `idpaket` int(11) NOT NULL,
  `idtravel` int(11) NOT NULL,
  `harga` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tawar_bursa`
--

INSERT INTO `tawar_bursa` (`id`, `idpaket`, `idtravel`, `harga`) VALUES
(1, 1, 1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `kode_booking` varchar(12) NOT NULL,
  `idpaket` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `total_biaya` int(11) DEFAULT NULL,
  `foto_bukti` varchar(100) DEFAULT NULL,
  `status` int(2) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `kode_booking`, `idpaket`, `iduser`, `total_biaya`, `foto_bukti`, `status`, `tanggal`) VALUES
(1, 'BK.0519.001', 1, 1, 250000000, NULL, 1, '2019-05-01'),
(86, '', 2, 1, NULL, NULL, 2, '0000-00-00'),
(87, '', 6, 1, NULL, NULL, 3, '0000-00-00'),
(88, '', 7, 1, NULL, NULL, 1, '0000-00-00'),
(89, '', 2, 1, NULL, NULL, 1, '0000-00-00'),
(90, '', 1, 1, NULL, NULL, 1, '0000-00-00'),
(91, '', 9, 1, NULL, NULL, 1, '0000-00-00'),
(92, '', 6, 1, NULL, NULL, 1, '0000-00-00'),
(93, '', 6, 1, NULL, NULL, 1, '0000-00-00'),
(94, '', 6, 1, NULL, NULL, 1, '0000-00-00'),
(95, '', 7, 1, NULL, NULL, 1, '0000-00-00'),
(96, '', 5, 1, NULL, NULL, 1, '0000-00-00'),
(97, '', 5, 1, NULL, NULL, 1, '0000-00-00'),
(98, '', 6, 1, NULL, NULL, 1, '0000-00-00'),
(99, '', 1, 1, NULL, NULL, 1, '0000-00-00'),
(100, '', 5, 1, NULL, NULL, 1, '0000-00-00'),
(101, '', 6, 1, NULL, NULL, 1, '0000-00-00'),
(107, '', 5, 3, 0, '5d05efd95db85.png', 3, '0000-00-00'),
(108, '', 8, 3, NULL, NULL, 2, '0000-00-00'),
(109, '', 6, 3, NULL, NULL, 4, '0000-00-00'),
(110, '', 7, 3, NULL, NULL, 2, '0000-00-00'),
(111, '', 6, 3, NULL, NULL, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_orang`
--

CREATE TABLE `transaksi_orang` (
  `id` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `jenkel` varchar(25) DEFAULT NULL,
  `nohp` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_orang`
--

INSERT INTO `transaksi_orang` (`id`, `id_transaksi`, `nama`, `jenkel`, `nohp`) VALUES
(1, 1, 'jujun', 'L', '0856323'),
(26, 86, 'masqqq', 'L', '31444'),
(27, 86, 'mamas', 'L', '0855'),
(28, 87, 'test', 'L', '0855'),
(29, 87, 'tassss', 'L', '4555'),
(30, 88, 'sau', 'P', '34144'),
(31, 88, 'test', 'L', '0855'),
(32, 89, 'test', 'L', '0854674'),
(33, 89, 'rorom', 'L', '054685'),
(34, 92, 'Jodi', 'L', '08564882'),
(35, 92, 'Nyanyag', 'L', '086434'),
(36, 93, 'test', 'L', '0558'),
(37, 94, 'test', 'L', ''),
(38, 96, 'sia', 'L', '0884'),
(39, 101, 'test', 'L', '0854858'),
(40, 107, 'test', 'L', '08564'),
(41, 108, 'test', 'L', '08566'),
(42, 108, 'test', 'L', '0856558'),
(43, 108, 'Ajat', 'P', '085640444'),
(44, 108, 'Mamad', 'L', '085647'),
(45, 110, 'Nani', 'P', '952888'),
(46, 110, 'Imas', 'P', '08564312'),
(47, 110, 'Maman', 'L', '08541582'),
(48, 110, 'udi ', 'L', '045852');

-- --------------------------------------------------------

--
-- Table structure for table `travel`
--

CREATE TABLE `travel` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_izin` varchar(25) NOT NULL,
  `nohp` varchar(13) NOT NULL,
  `email` varchar(25) DEFAULT NULL,
  `no_rekening` int(25) NOT NULL,
  `deskripsi` varchar(300) DEFAULT NULL,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(25) DEFAULT NULL,
  `foto_izin` varchar(50) DEFAULT NULL,
  `foto_logo` varchar(50) DEFAULT NULL,
  `rating` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `travel`
--

INSERT INTO `travel` (`id`, `nama`, `alamat`, `no_izin`, `nohp`, `email`, `no_rekening`, `deskripsi`, `username`, `password`, `foto_izin`, `foto_logo`, `rating`) VALUES
(1, 'Travel Barokah', 'Kota Depokk', '123456', '08587342', 'travelbarokah@gmail.com', 1234567, 'Barokah Pisan Euy', 'barokahtravel', 'barokahtravel', NULL, '5d05e1dabd247.png', 5),
(2, 'test', '', '', '', 'test@gmail.com', 0, NULL, 'test', 'test', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `jenkel` char(25) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `no_telp` varchar(13) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `tgl_dibuat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `alamat`, `jenkel`, `email`, `photo`, `no_telp`, `username`, `password`, `tgl_dibuat`) VALUES
(1, 'Angga', 'Jl Widyakrama', 'P', 'angga.riansah@gmail.com', NULL, '07565', 'angga', 'angga', '2019-05-11 10:33:46'),
(2, 'Test', 'test', 'L', 'a', 'http://thegorbalsla.com/wp-content/uploads/2018/08/Dieng-Plateau-Jawa-Tengah-700x467.jpg', '45', 'as', 'ss', '2019-06-11 06:08:48'),
(3, 'test', '0864', 'Laki - Laki', 'test@gmail.com', '5d07b48ac217c.png', '', 'test', 'test', '2019-06-17 15:40:58'),
(4, 'c', NULL, NULL, 'c', NULL, '', 'c', 'c', '2019-05-26 05:48:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpaket` (`idpaket`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idadmin` (`idadmin`);

--
-- Indexes for table `bursa`
--
ALTER TABLE `bursa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpaket` (`idpaket`),
  ADD KEY `status` (`status`),
  ADD KEY `pengambil` (`pengambil`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser` (`iduser`),
  ADD KEY `idadmin` (`idadmin`);

--
-- Indexes for table `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpaket` (`idpaket`),
  ADD KEY `iduser` (`iduser`);

--
-- Indexes for table `fcm_info`
--
ALTER TABLE `fcm_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto_paket`
--
ALTER TABLE `foto_paket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpaket` (`idpaket`);

--
-- Indexes for table `job_apply`
--
ALTER TABLE `job_apply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_request` (`id_request`),
  ADD KEY `id_muthawif` (`id_muthawif`);

--
-- Indexes for table `muthawif`
--
ALTER TABLE `muthawif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_travel` (`id_travel`);

--
-- Indexes for table `request_muthawif`
--
ALTER TABLE `request_muthawif`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpaket` (`idpaket`),
  ADD KEY `status_bayar` (`status_bayar`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idtravel` (`idtravel`),
  ADD KEY `iduser` (`iduser`);

--
-- Indexes for table `status_bayar`
--
ALTER TABLE `status_bayar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_paket`
--
ALTER TABLE `status_paket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_transaksi`
--
ALTER TABLE `status_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tawar_bursa`
--
ALTER TABLE `tawar_bursa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpaket` (`idpaket`),
  ADD KEY `iduser` (`iduser`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `transaksi_orang`
--
ALTER TABLE `transaksi_orang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_transaksi` (`id_transaksi`);

--
-- Indexes for table `travel`
--
ALTER TABLE `travel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `aktifitas`
--
ALTER TABLE `aktifitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bursa`
--
ALTER TABLE `bursa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `fcm_info`
--
ALTER TABLE `fcm_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `foto_paket`
--
ALTER TABLE `foto_paket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_apply`
--
ALTER TABLE `job_apply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `muthawif`
--
ALTER TABLE `muthawif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `request_muthawif`
--
ALTER TABLE `request_muthawif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `status_bayar`
--
ALTER TABLE `status_bayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `status_paket`
--
ALTER TABLE `status_paket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `status_transaksi`
--
ALTER TABLE `status_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tawar_bursa`
--
ALTER TABLE `tawar_bursa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `transaksi_orang`
--
ALTER TABLE `transaksi_orang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `travel`
--
ALTER TABLE `travel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD CONSTRAINT `aktifitas_ibfk_1` FOREIGN KEY (`idpaket`) REFERENCES `paket` (`id`);

--
-- Constraints for table `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`idadmin`) REFERENCES `admin` (`id`);

--
-- Constraints for table `bursa`
--
ALTER TABLE `bursa`
  ADD CONSTRAINT `bursa_ibfk_1` FOREIGN KEY (`idpaket`) REFERENCES `paket` (`id`),
  ADD CONSTRAINT `bursa_ibfk_2` FOREIGN KEY (`status`) REFERENCES `status_bayar` (`id`),
  ADD CONSTRAINT `bursa_ibfk_3` FOREIGN KEY (`pengambil`) REFERENCES `travel` (`id`);

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `chat_ibfk_2` FOREIGN KEY (`idadmin`) REFERENCES `admin` (`id`);

--
-- Constraints for table `favorite`
--
ALTER TABLE `favorite`
  ADD CONSTRAINT `favorite_ibfk_1` FOREIGN KEY (`idpaket`) REFERENCES `paket` (`id`),
  ADD CONSTRAINT `favorite_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`);

--
-- Constraints for table `foto_paket`
--
ALTER TABLE `foto_paket`
  ADD CONSTRAINT `foto_paket_ibfk_1` FOREIGN KEY (`idpaket`) REFERENCES `paket` (`id`);

--
-- Constraints for table `job_apply`
--
ALTER TABLE `job_apply`
  ADD CONSTRAINT `job_apply_ibfk_1` FOREIGN KEY (`id_request`) REFERENCES `request_muthawif` (`id`),
  ADD CONSTRAINT `job_apply_ibfk_2` FOREIGN KEY (`id_muthawif`) REFERENCES `muthawif` (`id`);

--
-- Constraints for table `paket`
--
ALTER TABLE `paket`
  ADD CONSTRAINT `paket_ibfk_1` FOREIGN KEY (`id_travel`) REFERENCES `travel` (`id`);

--
-- Constraints for table `request_muthawif`
--
ALTER TABLE `request_muthawif`
  ADD CONSTRAINT `request_muthawif_ibfk_1` FOREIGN KEY (`idpaket`) REFERENCES `paket` (`id`),
  ADD CONSTRAINT `request_muthawif_ibfk_2` FOREIGN KEY (`status_bayar`) REFERENCES `status_bayar` (`id`);

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`idtravel`) REFERENCES `travel` (`id`),
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`idpaket`) REFERENCES `paket` (`id`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`status`) REFERENCES `status_transaksi` (`id`);

--
-- Constraints for table `transaksi_orang`
--
ALTER TABLE `transaksi_orang`
  ADD CONSTRAINT `transaksi_orang_ibfk_1` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
