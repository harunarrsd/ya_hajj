<?php
  $chat = $this->db->query("SELECT * FROM chat a JOIN user b ON a.iduser = b.id WHERE role = 'User' GROUP BY a.iduser");
?>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
        <li class="<?php if($this->uri->segment(1) == 'dashboard') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("dashboard")?>">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard</span>
          </a>
        </li>
      <li class="header">DATA CORE</li>
        <li class="<?php if($this->uri->segment(1) == 'admin') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("admin")?>">
            <i class="fa fa-user" style="file"></i>
            <span>Admin</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'banner') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("banner")?>">
            <i class="fa fa-image" style="file"></i>
            <span>Banner</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'blog') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("blog")?>">
            <i class="fa fa-newspaper-o" style="file"></i>
            <span>Blog</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'chat') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("chat")?>">
            <i class="fa fa-envelope" style="file"></i>
            <span>Chat</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $chat->num_rows();?></small>
            </span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'muthawif_data') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("muthawif_data")?>">
            <i class="fa fa-sticky-note"></i>
            <span>Muthawif</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'travel') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("travel")?>">
            <i class="fa fa-book"></i>
            <span>Travel</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'user') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("user")?>">
            <i class="fa fa-users "></i>
            <span>User</span>
          </a>
        </li>
      <li class="header">TRANSAKSI</li>
        <li class="<?php if($this->uri->segment(1) == 'bursa') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("bursa")?>">
            <i class="fa fa-file-text"></i>
            <span>Bursa</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'mutawif') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("mutawif")?>">
            <i class="fa fa-sticky-note"></i>
            <span>Muthawif</span>
          </a>
        </li>
        <li class="<?php if($this->uri->segment(1) == 'paket') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("paket")?>">
            <i class="fa fa-file"></i>
            <span>Paket</span>
          </a>
        </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>