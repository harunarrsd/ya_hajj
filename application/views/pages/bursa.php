<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Data Transaksi Bursa
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li class="active">Transaksi Bursa</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            
        </div>
        <div class="box-body">
            <table id="example1" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Paket</th>
                        <th>Travel Pengambil</th>
                        <th>Pemilik Paket</th>
                        <th>Harga Jamaah</th>
                        <th>Total Bayar</th>
                        <th>Foto Bukti</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>

                <tbody>
                <?php
                    $no=0;
                    foreach ($main['sql']->result() as $obj)
                    {
                        $id_transaksi = $obj->id_transaksi;
                        $no++;
                ?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $obj->nama_paket;?></td>
                        <td></td>
                        <td></td>
                        <td><?php echo $obj->harga_jamaah;?></td>
                        <td><?php echo $obj->total_bayar;?></td>
                        <td>
                            <a href="#" data-toggle="modal" data-target=".gambar<?php echo $id_transaksi;?>">
                                <img src="<?php if($obj->gambar_bursa==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/bursa/$obj->gambar_bursa");?>" width="100%">
                            </a>
                        </td>
                        <td align="center">
                            <?php
                            if ($obj->status_bursa==1){
                            ?>
                                <label class="label label-danger">Belum Bayar</label>
                            <?php 
                            } else if ($obj->status_bursa==2) {
                            ?>
                                <label class="label label-warning">Konfirmasi</label>
                            <?php
                            } else if ($obj->status_bursa==3) {
                            ?>
                                <label class="label label-success">Berhasil</label>
                            <?php
                            } else{
                            ?>
                                <label>Kosong</label>
                            <?php
                            }
                            ?>
                        </td>                      
                        <td align="center">
                            <?php 
                            if ($obj->status_bursa==2) {
                            ?>
                                <a class="btn btn-xs btn-success" href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>/bursa/konfirmasi/<?php echo $obj->id_transaksi;?>';}"><i class="fa fa-check"></i> Terima</a>
                            <?php
                            }
                            ?>
                            
                        </td>                        
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<?php
    foreach ($main['sql']->result() as $obj)
    {
        $id_transaksi = $obj->id_transaksi;
?>
<div class="modal fade gambar<?php echo $id_transaksi;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <img src="<?php if($obj->gambar_bursa==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/bursa/$obj->gambar_bursa");?>" width="100%">
      </div>
    </div>
  </div>
</div>
<?php
}
?>