<?php
    foreach ($main['sql']->result() as $sql) {
        $id = $sql->id;
        $nama = $sql->nama;
        $username = $sql->username;
        $no_ktp = $sql->no_ktp;
        $no_paspor = $sql->no_paspor;
        $email = $sql->email;
        $no_telp = $sql->no_telp;
        $tgl_lahir = $sql->tgl_lahir;
        $alamat = $sql->alamat;
        $password = $sql->password;
        $gambar = $sql->gambar;
    }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Data Detail Muthawif
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo site_url("muthawif_data")?>"></i>Muthawif</a></li>
      <li class="active">Detail Muthawif</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            <a href="<?php echo site_url('muthawif_data');?>" class="btn btn-hajj"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info" style="padding:10px;">
                                <img id="image-preview" alt="image preview" width="100%" src="<?php if($gambar==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/muthawif/$gambar");?>"/>
                            </div>
                            <div class="text-center">
                                <label>Gambar</label>
                            </div>
                        </div>
                        <!-- <div class="col-md-6"></div> -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <label class="col-md-5">Nama Muthawif</label>
                        <div class="col-md-7">: <?php echo $nama;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">Alamat</label>
                        <div class="col-md-7">: <?php echo $alamat;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">No.KTP</label>
                        <div class="col-md-7">: <?php echo $no_ktp;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">No.Paspor</label>
                        <div class="col-md-7">: <?php echo $no_paspor;?></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <label class="col-md-5">No.Telepon</label>
                        <div class="col-md-7">: <?php echo $no_telp;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">Email</label>
                        <div class="col-md-7">: <?php echo $email;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">Username</label>
                        <div class="col-md-7">: <?php echo $username;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">Password</label>
                        <div class="col-md-7">: <?php echo $password;?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>