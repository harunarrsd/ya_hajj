<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Data Muthawif
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li class="active">Muthawif</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            <a href="<?php echo site_url('muthawif_data/form')?>" class="btn btn-hajj"><i class="fa fa-plus"></i> Tambah </a>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>No. KTP</th>
                        <th>Alamat</th>
                        <th>No.Paspor</th>
                        <th>No.Telepon</th>
                        <th>Username</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                <?php
                    $no=0;
                    foreach ($main['sql']->result() as $obj)
                    {
                        $no++;
                ?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $obj->nama;?></td>
                        <td><?php echo $obj->no_ktp;?></td>
                        <td><?php echo $obj->alamat;?></td>
                        <td><?php echo $obj->no_paspor;?></td>
                        <td><?php echo $obj->no_telp;?></td>
                        <td><?php echo $obj->username;?></td>
                        <td>
                            <select class="form-control" onchange="location = this.value;">
                                <option value="<?php echo site_url("muthawif_data");?>">Action</option>
                                <option value="<?php echo site_url("muthawif_data/form_edit/$obj->id");?>">Ubah</option>
                                <option value="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>/muthawif_data/delete/<?php echo $obj->id;?>';}">Hapus</option>
                                <option value="<?php echo site_url("muthawif_data/detail/$obj->id");?>">Detail</option>
                                <option value="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>muthawif_data/reset_password/<?php echo $obj->id;?>';}">Reset Password</option>
                            </select>
                        </td>
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>