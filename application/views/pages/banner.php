<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Data Banner
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li class="active">Banner</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            <a href="<?php echo site_url('banner/form')?>" class="btn btn-hajj"><i class="fa fa-plus"></i> Tambah </a>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th width="150">Judul</th>
                        <th>Gambar</th>
                        <th width="130">Status</th>
                        <th width="112">Action</th>
                    </tr>
                </thead>

                <tbody>
                <?php
                    $no=0;
                    foreach ($main['sql']->result() as $obj)
                    {
                        $id = $obj->id;
                        $no++;
                ?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $obj->judul;?></td>
                        <td>
                            <a href="#" data-toggle="modal" data-target=".gambar<?php echo $id;?>">
                                <img src="<?php if($obj->gambar==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/banner/$obj->gambar");?>" width="100%">
                            </a>
                        </td>
                        <td align="center">
                            <?php
                            if ($obj->status=="Aktif") {
                            ?>
                                <label class="label label-success">Aktif</label>
                            <?php
                            } else{
                            ?>
                                <label class="label label-danger">Tidak Aktif</label>
                            <?php
                            }
                            ?>
                        </td>
                        <td>
                            <a class="btn btn-xs btn-info" href="<?php echo site_url();?>banner/form_edit/<?php echo $id;?>"><i class='fa fa-edit'></i> Ubah</a> 
                            <?php
                            if ($obj->status=="Aktif") {
                            ?>
                                <a  class="btn btn-xs btn-danger" href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>/banner/konfirmasi_tidak_aktif/<?php echo $id;?>';}"><i class='fa fa-close'></i> Tidak Aktif</a>
                            <?php
                            } else{
                            ?>
                                <a  class="btn btn-xs btn-success" href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>/banner/konfirmasi_aktif/<?php echo $id;?>';}"><i class='fa fa-check'></i> Aktif</a>
                            <?php
                            }
                            ?>
                        </td>
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<!-- Modal Ganti Password -->
<?php
    foreach ($main['sql']->result() as $obj)
    {
        $id = $obj->id;
?>
<div class="modal fade gambar<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <img src="<?php if($obj->gambar==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/banner/$obj->gambar");?>" width="100%">
      </div>
    </div>
  </div>
</div>
<?php
}
?>