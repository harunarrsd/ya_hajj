<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Dashboard
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
</section>

  <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $main['user']->num_rows();?></h3>
              <p>User</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-person"></i> -->
              <i class="fa fa-users "></i>
            </div>
            <a href="<?php echo site_url('user');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $main['travel']->num_rows();?></h3>
              <p>Travel</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-journal"></i> -->
              <i class="fa fa-book"></i>
            </div>
            <a href="<?php echo site_url('travel');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $main['paket']->num_rows();?></h3>
              <p>Paket</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-document"></i> -->
              <i class="fa fa-file"></i>
            </div>
            <a href="<?php echo site_url('paket');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $main['muthawif']->num_rows();?></h3>
              <p>Muthawif</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-pie-graph"></i> -->
              <i class="fa fa-sticky-note"></i>
            </div>
            <a href="<?php echo site_url('mutawif');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $main['admin']->num_rows();?></h3>
              <p>Admin</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-person"></i> -->
              <i class="fa fa-user"></i>
            </div>
            <a href="<?php echo site_url('admin');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $main['banner']->num_rows();?></h3>
              <p>Banner</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-person"></i> -->
              <i class="fa fa-image"></i>
            </div>
            <a href="<?php echo site_url('banner');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $main['bursa']->num_rows();?></h3>
              <p>Bursa</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-person"></i> -->
              <i class="fa fa-file-text"></i>
            </div>
            <a href="<?php echo site_url('bursa');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $main['blog']->num_rows();?></h3>
              <p>Blog</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-person"></i> -->
              <i class="fa fa-newspaper-o"></i>
            </div>
            <a href="<?php echo site_url('blog');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
    </section>