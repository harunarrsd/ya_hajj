<?php
  $id= "";
  $nama = "";
  $username = "";
  $no_ktp = "";
  $no_paspor = "";
  $email = "";
  $no_telp = "";
  $tgl_lahir = "";
  $alamat = "";
  $password = "";
  $gambar = "";
  $tanggal = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $sql) {
      $op = "edit";
      $id = $sql->id;
      $nama = $sql->nama;
      $username = $sql->username;
      $no_ktp = $sql->no_ktp;
      $no_paspor = $sql->no_paspor;
      $email = $sql->email;
      $no_telp = $sql->no_telp;
      $tgl_lahir = $sql->tgl_lahir;
      $tanggal = date('Y-m-d', strtotime($tgl_lahir));
      $alamat = $sql->alamat;
      $password = $sql->password;
      $gambar = $sql->gambar;
    }
  }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Form Muthawif
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo site_url("muthawif_data")?>">Muthawif</a></li>
      <li class="active">Form Muthawif</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border">

        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('muthawif_data/create/');?>
          <input type="hidden" name="op" value="<?php echo $main['op'];?>">
          <input type="hidden" name="id" value="<?php echo $id;?>">
          <?php foreach($main['last']->result() as $id) $last = $id->id+1; $last_edit = $id->id;?>
          <input type="hidden" name="id_last" value="<?php if($main['op']=='edit') echo $last_edit; else echo $last;?>">
          <div class="box-body form-horizontal">
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Nama Muthawif</label>
                <div class="col-sm-10">
                    <input type="text" name="nama" value="<?php echo $nama;?>" class="form-control" id="inputName" placeholder="Nama Muthawif" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                  <input type="text" name="username" value="<?php echo $username;?>" class="form-control" id="inputName" placeholder="Username" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="alamat" cols="30" rows="10" placeholder="Alamat" required><?php echo $alamat; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Tanggal Lahir</label>
                <div class="col-sm-10">
                    <input type="date" name="tgl_lahir" value="<?php echo $tanggal;?>" class="form-control"placeholder="Tanggal Lahir" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" name="email" value="<?php echo $email;?>" class="form-control"placeholder="Email" required>
                </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">No.KTP</label>
              <div class="col-sm-10">
                  <input type="number" name="no_ktp" value="<?php echo $no_ktp;?>" class="form-control" placeholder="No.KTP" required>
              </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">No.Paspor</label>
                <div class="col-sm-10">
                    <input type="number" name="no_paspor" value="<?php echo $no_paspor;?>" class="form-control" placeholder="No.Paspor" required>
                </div>
            </div>
            <?php
              if ($main['op']=="tambah") {
            ?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <input type="password" name="password" value="<?php echo $password;?>" class="form-control" placeholder="Password" required>
                </div>
              </div>
            <?php
              }
            ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">No.Telp</label>
                <div class="col-sm-10">
                    <input type="text" name="no_telp" value="<?php echo $no_telp;?>" class="form-control" placeholder="No.Telp" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Gambar</label>
                <div class="col-sm-10">
                    <input type="file" name="gambar" value="<?php echo $gambar;?>" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <div class="col-sm-2 control-label"></div>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-hajj">Submit</button>
                <a href="<?php echo site_url('muthawif_data')?>"class="btn btn-danger" style="color:white;">Kembali</a>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </form>
    </div>
          <!-- /.box -->
</section>
  <!-- /.content -->