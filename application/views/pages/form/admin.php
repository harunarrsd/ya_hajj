<?php
  $id= "";
  $nama = "";
  $username = "";
  $password = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $obj) {
      $op = "edit";
      $id = $obj->id;
      $nama = $obj->nama;
      $username = $obj->username;
      $password = $obj->password;
    }
  }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Form Admin
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo site_url("admin")?>">Admin</a></li>
      <li class="active">Form Admin</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">Form User</h3> -->
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('admin/create/');?>
            <input type="hidden" name="op" value="<?php echo $main['op'];?>">
            <input type="hidden" name="id" value="<?php echo $id;?>">
            <div class="box-body form-horizontal">
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" name="nama" value="<?php echo $nama;?>" class="form-control" id="inputName" placeholder="Nama" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" name="username" value="<?php echo $username;?>" class="form-control" id="inputEmail3" placeholder="Username" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                    <?php if ($main['op']=="edit") {
                    ?>
                      <div class="col-sm-8">
                        <input disabled type="password" name="password" value="<?php echo $password;?>" class="form-control" id="inputPassword3" placeholder="Password" required>
                      </div>
                      <div class="col-sm-2">
                        <a href="#" class="btn btn-hajj" data-toggle="modal" data-target=".ganti_password<?php echo $id;?>">Ganti Password</a>
                      </div>
                    <?php
                      } else {
                    ?>
                      <div class="col-sm-10">
                        <input type="password" name="password" value="<?php echo $password;?>" class="form-control" id="inputPassword3" placeholder="Password" required>
                      </div>
                    <?php
                      }
                    ?>
                </div>
                
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-hajj">Submit</button>
                    <a href="<?php echo site_url('admin')?>"class="btn btn-danger" style="color:white;">Kembali</a>
                  </div>
                </div>
            </div>
            <!-- /.box-body -->
        </form>
    </div>
          <!-- /.box -->
</section>
  <!-- /.content -->

<!-- Modal Ganti Password -->
<div class="modal fade ganti_password<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <?php echo form_open_multipart('admin/cek_password/');?>
      <input type="hidden" name="id" value="<?php echo $id;?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Ganti Password</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="inputName" class="col-sm-4 control-label">Password Lama</label>
            <div class="col-sm-8">
                <input type="password" name="password_lama" class="form-control" placeholder="Password Lama" required>
            </div>
        </div><br><br>
        <div class="form-group">
            <label for="inputName" class="col-sm-4 control-label">Password Baru</label>
            <div class="col-sm-8">
                <input type="password" id="pw" name="password_baru" class="form-control" placeholder="Password Baru" required>
            </div>
        </div><br><br>
        <div class="form-group">
            <label for="inputName" class="col-sm-4 control-label">Ulangi Password Baru</label>
            <div class="col-sm-8">
                <input type="password" id="re_pw" name="ulangi_password_baru" class="form-control" placeholder="Ulangi Password Baru" required>
            </div>
        </div>
      </div><br>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" id="btnSubmit" class="btn btn-hajj">Simpan</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function () {
            var password = $("#pw").val();
            var confirmPassword = $("#re_pw").val();
            if (password != confirmPassword) {
                alert("Ulangi password baru tidak cocok. Harap ketik ulang password baru!");
                return false;
            }
            return true;
        });
    });
</script>