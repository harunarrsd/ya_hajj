<?php
  $id= "";
  $judul = "";
  $gambar = "";
  $status = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $sql) {
      $op = "edit";
      $id = $sql->id;
      $judul = $sql->judul;
      $gambar = $sql->gambar;
      $status = $sql->status;
    }
  }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Form Banner
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo site_url("banner")?>">Banner</a></li>
      <li class="active">Form Banner</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border">

        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('banner/create/');?>
          <input type="hidden" name="op" value="<?php echo $main['op'];?>">
          <input type="hidden" name="id" value="<?php echo $id;?>">
          <?php foreach($main['last']->result() as $id) $last = $id->id+1;?>
          <?php
          if ($main['op']=='tambah') {
          ?>
            <input type="hidden" name="id_last" value="<?php echo $last;?>">
          <?php
          }
          ?>
          <!-- <input type="text" name="id_last" value="<?php if($main['op']=='edit') echo $id; else echo $last;?>"> -->
          <div class="box-body form-horizontal">
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Judul Banner</label>
                <div class="col-sm-10">
                    <input type="text" name="judul" value="<?php echo $judul;?>" class="form-control" id="inputName" placeholder="Judul Banner" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Gambar Banner</label>
                <div class="col-sm-10">
                    <input type="file" name="gambar" value="<?php echo $gambar;?>" class="form-control" <?php if($main['op']=='tambah') echo 'required'?>>
                </div>
            </div>
            <?php
            if ($main['op']=='tambah') {
            ?>
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Status</label>
                <div class="col-sm-10">
                    <select name="status" class="form-control">
                      <option value="">Pilih</option>
                      <option value="Aktif" <?php if($status=="Aktif") echo 'selected'?>>Aktif</option>
                      <option value="Tidak Aktif" <?php if($status=="Tidak Aktif") echo 'selected'?>>Tidak Aktif</option>
                    </select>
                </div>
              </div>
            <?php
            }
            ?>
            <div class="form-group">
              <div class="col-sm-2 control-label"></div>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-hajj">Submit</button>
                <a href="<?php echo site_url('banner')?>"class="btn btn-danger" style="color:white;">Kembali</a>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </form>
    </div>
          <!-- /.box -->
</section>
  <!-- /.content -->