<?php
  $id= "";
  $judul = "";
  $deskripsi = "";
  $gambar = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $sql) {
      $op = "edit";
      $id = $sql->id;
      $judul = $sql->judul;
      $deskripsi = $sql->deskripsi;
      $gambar = $sql->gambar;
    }
  }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Form Blog
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo site_url("blog")?>">Blog</a></li>
      <li class="active">Form Blog</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border">

        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('blog/create/');?>
          <input type="hidden" name="op" value="<?php echo $main['op'];?>">
          <input type="hidden" name="id" value="<?php echo $id;?>">
          <?php foreach($main['last']->result() as $id) $last = $id->id+1;?>
          <?php
          if ($main['op']=='tambah') {
          ?>
            <input type="hidden" name="id_last" value="<?php echo $last;?>">
          <?php
          }
          ?>
          <div class="box-body form-horizontal">
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Judul Blog</label>
                <div class="col-sm-10">
                    <input type="text" name="judul" value="<?php echo $judul;?>" class="form-control" id="inputName" placeholder="Judul Banner" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Deskripsi Blog</label>
                <div class="col-sm-10">
                    <textarea id="editor1" name="deskripsi"><?php echo $deskripsi;?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Gambar Blog</label>
                <div class="col-sm-10">
                    <input type="file" name="gambar" value="<?php echo $gambar;?>" class="form-control" <?php if($main['op']=='tambah') echo 'required'?>>
                </div>
            </div>
            <div class="form-group">
              <div class="col-sm-2 control-label"></div>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-hajj">Submit</button>
                <a href="<?php echo site_url('blog')?>"class="btn btn-danger" style="color:white;">Kembali</a>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </form>
    </div>
          <!-- /.box -->
</section>
  <!-- /.content -->