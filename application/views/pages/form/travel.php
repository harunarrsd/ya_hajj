<?php
  $id= "";
  $nama = "";
  $alamat = "";
  $deskripsi = "";
  $nohp = "";
  $email = "";
  $no_izin = "";
  $no_rekening = "";
  $username = "";
  $password = "";
  $foto_izin = "";
  $foto_logo = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $sql) {
      $op = "edit";
      $id = $sql->id;
      $nama = $sql->nama;
      $deskripsi = $sql->deskripsi;
      $alamat = $sql->alamat;
      $nohp = $sql->nohp;
      $email = $sql->email;
      $no_izin = $sql->no_izin;
      $no_rekening = $sql->no_rekening;
      $username = $sql->username;
      $password = $sql->password;
      $foto_izin = $sql->foto_izin;
      $foto_logo = $sql->foto_logo;
    }
  }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Form Travel
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo site_url("travel")?>">Travel</a></li>
      <li class="active">Form Travel</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border">

        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('travel/create/');?>
          <input type="hidden" name="op" value="<?php echo $main['op'];?>">
          <input type="hidden" name="id" value="<?php echo $id;?>">
          <?php foreach($main['last']->result() as $id) $last = $id->id+1; $last_edit = $id->id;?>
          <input type="hidden" name="id_last" value="<?php if($main['op']=='edit') echo $last_edit; else echo $last;?>">
          <div class="box-body form-horizontal">
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Nama Travel</label>
                <div class="col-sm-10">
                    <input type="text" name="nama" value="<?php echo $nama;?>" class="form-control" id="inputName" placeholder="Nama Travel" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Deskripsi Travel</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="deskripsi" cols="30" rows="10" placeholder="Deskripsi Travel" required><?php echo $deskripsi; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Alamat Travel</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="alamat" cols="30" rows="10" placeholder="Alamat Travel" required><?php echo $alamat; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Email Travel</label>
                <div class="col-sm-10">
                    <input type="email" name="email" value="<?php echo $email;?>" class="form-control"placeholder="Email Travel" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Foto Logo</label>
                <div class="col-sm-10">
                    <input type="file" name="foto_logo" value="<?php echo $foto_logo;?>" class="form-control" <?php if($main['op']=='tambah') echo 'required'?>>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">No.Handphone</label>
                <div class="col-sm-10">
                    <input type="number" name="nohp" value="<?php echo $nohp;?>" class="form-control" placeholder="No.Handphone" required>
                </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">No.Rekening</label>
              <div class="col-sm-10">
                  <input type="number" name="no_rekening" value="<?php echo $no_rekening;?>" class="form-control" placeholder="No.Rekening" required>
              </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                    <input type="text" name="username" value="<?php echo $username;?>" class="form-control" placeholder="Username" required>
                </div>
            </div>
            <?php
              if ($main['op']=="tambah") {
            ?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <input type="password" name="password" value="<?php echo $password;?>" class="form-control" placeholder="Password" required>
                </div>
              </div>
            <?php
              }
            ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">No.Izin</label>
                <div class="col-sm-10">
                    <input type="text" name="no_izin" value="<?php echo $no_izin;?>" class="form-control" placeholder="No.Izin" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Foto Izin</label>
                <div class="col-sm-10">
                    <input type="file" name="foto_izin" value="<?php echo $foto_izin;?>" class="form-control" <?php if($main['op']=='tambah') echo 'required'?>>
                </div>
            </div>
            <div class="form-group">
              <div class="col-sm-2 control-label"></div>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-hajj">Submit</button>
                <a href="<?php echo site_url('travel')?>"class="btn btn-danger" style="color:white;">Kembali</a>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </form>
    </div>
          <!-- /.box -->
</section>
  <!-- /.content -->