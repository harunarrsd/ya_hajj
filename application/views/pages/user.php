<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Data User
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li class="active">User</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            
        </div>
        <div class="box-body">
            <table id="example1" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Jenis Kelamin</th>
                        <th>Email</th>
                        <th>No.Telpon</th>
                    </tr>
                </thead>

                <tbody>
                <?php
                    $no=0;
                    foreach ($main['sql']->result() as $obj)
                    {
                        $no++;
                ?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $obj->nama;?></td>
                        <td><?php echo $obj->alamat;?></td>
                        <td>
                            <?php if($obj->jenkel=='L') {
                                echo 'Laki - laki';
                            } else if($obj->jenkel=='P'){
                                echo 'Perempuan';
                            }else{
                                echo 'Tidak memilih';
                            }
                            ?>
                        </td>
                        <td><?php echo $obj->email;?></td>
                        <td><?php echo $obj->no_telp;?></td>
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>