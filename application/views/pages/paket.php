<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Data Transaksi Paket
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li class="active">Transaksi Paket</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            
        </div>
        <div class="box-body">
            <table id="example1" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Paket</th>
                        <th>Travel</th>
                        <th>Pemesan</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total Biaya</th>
                        <th>Foto Bukti</th>
                        <th>Informasi</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>

                <tbody>
                <?php
                    $no=0;
                    foreach ($main['sql']->result() as $obj)
                    {
                        $id_transaksi = $obj->id_transaksi;
                        $no++;
                ?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $obj->nama_paket;?></td>
                        <td><?php echo $obj->nama_travel;?></td>
                        <td><?php echo $obj->nama_user;?></td>
                        <td>Rp<?php echo $obj->harga;?></td>
                        <td><?php echo $obj->jumlah_orang;?></td>
                        <td><?php echo $obj->total_biaya;?></td>
                        <td>
                            <a href="#" data-toggle="modal" data-target=".gambar<?php echo $id_transaksi;?>">
                                <img src="<?php if($obj->gambar_paket==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/paket/$obj->gambar_paket");?>" width="100%">
                            </a>
                        </td>
                        <td>
                            <a href="#" data-toggle="modal" data-target=".informasi<?php echo $id_transaksi;?>">
                                <img src="<?php if($obj->foto_ss==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/paket/$obj->foto_ss");?>" width="100%">
                            </a>
                        </td>
                        <td>
                            <?php
                            if ($obj->status_transaksi==1){
                            ?>
                                <label class="label label-danger">Belum Bayar</label>
                            <?php 
                            } else if ($obj->status_transaksi==2) {
                            ?>
                                <label class="label label-warning">Proses Konfirmasi</label>
                            <?php
                            } else if ($obj->status_transaksi==3) {
                            ?>
                                <label class="label label-warning">Konfirmasi</label>
                            <?php
                            } else if ($obj->status_transaksi==4) {
                            ?>
                                <label class="label label-success">Berhasil</label>
                            <?php
                            } else{
                            ?>
                                <label>Kosong</label>
                            <?php
                            }
                            ?>
                        </td>                      
                        <td>
                            <a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".upload<?php echo $id_transaksi;?>"><i class="fa fa-image"></i> Upload</a><br><br>
                            <?php 
                            if ($obj->status_transaksi==3) {
                            ?>
                                <a class="btn btn-xs btn-success" href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>/paket/konfirmasi/<?php echo $obj->id_transaksi;?>';}"><i class="fa fa-check"></i> Terima</a>
                            <?php
                            }
                            ?>
                        </td>                        
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<!-- Modal FOto Bukti -->
<?php
    foreach ($main['sql']->result() as $obj)
    {
        $id_transaksi = $obj->id_transaksi;
?>
<div class="modal fade gambar<?php echo $id_transaksi;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <img src="<?php if($obj->gambar_paket==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/paket/$obj->gambar_paket");?>" width="100%">
      </div>
    </div>
  </div>
</div>
<?php
}
?>

<!-- Upload Foto -->
<?php
    foreach ($main['sql']->result() as $obj)
    {
        $id_transaksi = $obj->id_transaksi;
?>
<div class="modal fade upload<?php echo $id_transaksi;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
    <?php echo form_open_multipart('paket/update_foto');?>
    <input type="hidden" name="id" value="<?php echo $id_transaksi;?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>Upload Gambar</h4>
      </div>
      <div class="modal-body">
        <input class="form-control" type="file" name="foto_ss">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-info">Simpan</button>
      </div>
    <?php echo form_close();?>
    </div>
  </div>
</div>
<?php
}
?>

<!-- Modal Informasi -->
<?php
    foreach ($main['sql']->result() as $obj)
    {
        $id_transaksi = $obj->id_transaksi;
?>
<div class="modal fade informasi<?php echo $id_transaksi;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <img src="<?php if($obj->foto_ss==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/paket/$obj->foto_ss");?>" width="100%">
      </div>
    </div>
  </div>
</div>
<?php
}
?>