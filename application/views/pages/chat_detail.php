<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Detail Chat
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li class="active">Chat</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">

        </div>
        <div class="box-body">
          <?php foreach ($main['sql']->result() as $sql) {
            $iduser = $sql->iduser;
          ?>
            <div class="item">
              <p class="message">
                <a class="name">
                  <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?php echo $sql->waktu;?></small>
                 <b><?php echo $sql->nama;?></b>
                </a><br>
                <?php echo $sql->deskripsi;?>
              </p>
            </div>
          <?php
          }
          ?>
          <?php foreach ($main['sql2']->result() as $sql2) {
          ?>
            <div class="item">
              <p class="message">
                <a class="name">
                  <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?php echo $sql2->waktu;?></small>
                 <b><?php echo $sql2->nama;?></b>
                </a><br>
                <?php echo $sql2->deskripsi;?>
              </p>
            </div>
          <?php
          }
          ?>
        </div>
        <div class="box-footer">
          <?php echo form_open_multipart('chat/create/');?>
          <div class="input-group">
            <input type="hidden" name="id_user" value="<?php echo $iduser;?>">
              <input class="form-control" placeholder="Ketik Pesan..." name="deskripsi">
              <div class="input-group-btn">
                <button type="submit" class="btn btn-success">Send</button>
              </div>
          </div>
          </form>
        </div>
    </div>
</section>