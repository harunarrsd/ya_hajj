<?php
    foreach ($main['sql']->result() as $sql) {
        $id = $sql->id;
        $nama = $sql->nama;
        $deskripsi = $sql->deskripsi;
        $alamat = $sql->alamat;
        $nohp = $sql->nohp;
        $email = $sql->email;
        $no_izin = $sql->no_izin;
        $no_rekening = $sql->no_rekening;
        $username = $sql->username;
        $password = $sql->password;
        $foto_izin = $sql->foto_izin;
        $foto_logo = $sql->foto_logo;
    }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Data Detail Travel
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo site_url("travel")?>"><i class="fa fa-home"></i> Travel</a></li>
      <li class="active">Detail Travel</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            <a href="<?php echo site_url('travel');?>" class="btn btn-hajj"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info" style="padding:10px;">
                                <img id="image-preview" alt="image preview" width="100%" src="<?php if($foto_logo==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/travel/logo/$foto_logo");?>"/>
                            </div>
                            <div class="text-center">
                                <label>Logo</label>
                            </div>
                        </div>
                        <!-- <div class="col-md-6"></div> -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <label class="col-md-5">Nama Travel</label>
                        <div class="col-md-7">: <?php echo $nama;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">Deskripsi Travel</label>
                        <div class="col-md-7">: <?php echo $deskripsi;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">Alamat Travel</label>
                        <div class="col-md-7">: <?php echo $alamat;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">No. Handphone</label>
                        <div class="col-md-7">: <?php echo $nohp;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">Email</label>
                        <div class="col-md-7">: <?php echo $email;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">No. Izin</label>
                        <div class="col-md-7">: <?php echo $no_izin;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">No. Rekening</label>
                        <div class="col-md-7">: <?php echo $no_rekening;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">Username</label>
                        <div class="col-md-7">: <?php echo $username;?></div>
                    </div>
                    <div class="row">
                        <label class="col-md-5">Password</label>
                        <div class="col-md-7">: <?php echo $password;?></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <label class="col-md-3">Foto Izin :</label>
                        <div class="col-md-9">
                            <div class="panel panel-info" style="padding:10px;">
                                <img id="image-preview" alt="image preview" width="100%" src="<?php if($foto_izin==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/travel/izin/$foto_izin");?>"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>