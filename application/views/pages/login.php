<div class="login-box">
  <div class="login-logo">
    <img src="<?php echo base_url();?>assets/images/icon.png">
    <!-- <a href="."><b>Yaa</b> Hajj</a> -->
  </div>
  <div class="login-box-body">
    <br>
    <?php echo $this->session->flashdata('notif')?>
    <?php echo form_open('main/ceklogin')?>
    <form>
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Username" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8"></div>
        <div class="col-xs-4">
          <button type="submit" name="login" class="btn btn-hajj btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </form>
    <?php echo form_close()?>
  </div>
</div>