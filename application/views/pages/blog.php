<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Data Blog
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li class="active">Blog</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            <a href="<?php echo site_url('blog/form')?>" class="btn btn-hajj"><i class="fa fa-plus"></i> Tambah </a>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Judul</th>
                        <th>Deskripsi</th>
                        <th>Gambar</th>
                        <th>Author</th>
                        <th>Tanggal</th>
                        <th width="91">Action</th>
                    </tr>
                </thead>

                <tbody>
                <?php
                    $no=0;
                    foreach ($main['sql']->result() as $obj)
                    {
                        $idblog = $obj->id_blog;
                        $no++;
                ?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $obj->judul;?></td>
                        <td><?php echo word_limiter($obj->deskripsi, 25);?></td>
                        <td>
                            <a href="#" data-toggle="modal" data-target=".gambar<?php echo $idblog;?>">
                                <img src="<?php if($obj->gambar==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/blog/$obj->gambar");?>" width="100%">
                            </a>
                        </td>
                        <td><?php echo $obj->nama;?></td>
                        <td><?php echo $obj->tanggal;?></td>
                        <td>
                            <a class="btn btn-xs btn-info" href="<?php echo site_url();?>blog/form_edit/<?php echo $idblog;?>"><i class='fa fa-edit'></i> Ubah</a>
                            <a  class="btn btn-xs btn-danger" href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>/blog/delete/<?php echo $idblog;?>';}"><i class='fa fa-close'></i> Hapus</a>
                        </td>
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<!-- Modal Ganti Password -->
<?php
    foreach ($main['sql']->result() as $obj)
    {
        $idblog = $obj->id_blog;
?>
<div class="modal fade gambar<?php echo $idblog;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <img src="<?php if($obj->gambar==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/blog/$obj->gambar");?>" width="100%">
      </div>
    </div>
  </div>
</div>
<?php
}
?>