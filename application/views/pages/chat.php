<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Chat
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("dashboard")?>"><i class="fa fa-home"></i> Dashboard</a></li>
      <li class="active">Chat</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Pesan Masuk</h3>
        </div>
        <div class="box-body">
            <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                    <?php
                        foreach($main['sql']->result() as $sql){
                    ?>
                    <tr>
                        <td class="mailbox-name"><a href="<?php echo site_url('chat/chat_detail/');?><?php echo $sql->iduser;?>"><?php echo $sql->nama;?></a></td>
                        <td class="mailbox-subject"><b><?php echo $sql->deskripsi;?></b></td>
                        <td class="mailbox-date"><?php echo $sql->waktu;?></td>
                    </tr>
                    <?php
                        }
                    ?>
                  </tbody>
                </table>
                <!-- /.table -->
            </div>
        </div>
    </div>
</section>