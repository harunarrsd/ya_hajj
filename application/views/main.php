<html lang="en">
<head>
    <title><?php echo $main['title']; ?></title>
    <?php $this->load->view('layout/head') ?>
</head>
<body id="top" class="hold-transition sidebar-mini skin-green-light">

<div class="wrapper">

    <!-- header -->
    <?php $this->load->view('layout/header')?>
    <!-- END header -->

    <!-- sidebar -->
    <?php echo $main['sidebar']; ?>
    <!-- END sidebar -->

    <div class="content-wrapper">
        <!-- pages -->
        <?php echo $main['pages']; ?>
        <!-- END pages -->
    </div>
    
    <!-- footer -->
    <?php $this->load->view('layout/footer')?>
    <!-- END footer -->
</div>

    <!-- javascript -->
    <?php $this->load->view('layout/javascript')?>
    <!-- END javascript -->
</body>
</html>