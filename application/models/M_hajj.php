<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_hajj extends CI_Model{

    // login
    function proseslogin($user,$pass){
        $this->db->where('username',$user);
        $this->db->where('password',$pass);
        return $this->db->get('admin')->row();
    }

    // user
    function read_user(){
        $sql = $this->db->query("SELECT * FROM user");
        return $sql;
    }

    // admin
    function create_admin($data){
        $this->db->insert('admin',$data);
    }
    function read_admin(){
        $sql = $this->db->query("SELECT * FROM admin");
        return $sql;
    }
    function edit_admin($id){
        $this->db->where("id",$id);
        return $this->db->get('admin');
    }
    function update_admin($id,$data){
        $this->db->where("id",$id);
        $this->db->update('admin',$data);
    }
    function delete_admin($id){
        $this->db->where("id",$id);
        $this->db->delete("admin");
    }

    // Banner
    function create_banner($data){
        $this->db->insert('banner',$data);
    }
    function read_banner(){
        return $this->db->query("SELECT * FROM banner ORDER BY status ASC");
    }
    function edit_banner($id){
        $this->db->where("id",$id);
        return $this->db->get('banner');
    }
    function update_banner($id,$data){
        $this->db->where("id",$id);
        $this->db->update('banner',$data);
    }

    // Blog
    function create_blog($data){
        $this->db->insert('blog',$data);
    }
    function read_blog(){
        return $this->db->query("SELECT *, a.id as id_blog FROM blog a JOIN admin b ON a.idadmin = b.id");
    }
    function edit_blog($id){
        $this->db->where("id",$id);
        return $this->db->get('blog');
    }
    function update_blog($id,$data){
        $this->db->where("id",$id);
        $this->db->update('blog',$data);
    }
    function delete_blog($id){
        $this->db->where("id", $id);
        $query = $this->db->get('blog');
        $row = $query->row();
        unlink("upload/blog/$row->gambar");
        $this->db->delete('blog', array('id' => $id));
    }

    // Chat
    function create_chat($data){
        $this->db->insert('chat',$data);
    }
    function read_chat(){
        return $this->db->query("SELECT * FROM chat a JOIN user b ON a.iduser = b.id WHERE role = 'User' GROUP BY a.iduser");
    }

    // Travel
    function create_travel($data){
        $this->db->insert('travel',$data);
    }
    function read_travel(){
        $sql = $this->db->query("SELECT * FROM travel");
        return $sql;
    }
    function edit_travel($id){
        $this->db->where("id",$id);
        return $this->db->get('travel');
    }
    function update_travel($id,$data){
        $this->db->where("id",$id);
        $this->db->update('travel',$data);
    }
    function delete_travel($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get('travel');
        $row = $query->row();
        unlink("upload/travel/logo/$row->foto_logo");
        unlink("upload/travel/izin/$row->foto_izin");
        $this->db->delete('travel', array('id' => $id));
    }

    // Muthawif
    function create_muthawif_data($data){
        $this->db->insert('muthawif',$data);
    }
    function read_muthawif_data(){
        $sql = $this->db->query("SELECT * FROM muthawif");
        return $sql;
    }
    function edit_muthawif_data($id){
        $this->db->where("id",$id);
        return $this->db->get('muthawif');
    }
    function update_muthawif_data($id,$data){
        $this->db->where("id",$id);
        $this->db->update('muthawif',$data);
    }
    function delete_muthawif_data($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get('muthawif');
        $row = $query->row();
        unlink("upload/muthawif/$row->gambar");
        $this->db->delete('muthawif', array('id' => $id));
    }

    // Transaksi Paket
    function read_paket(){
        $sql = $this->db->query("SELECT *, b.nama as nama_paket, c.nama as nama_user, d.nama as nama_travel, a.status as status_transaksi, a.id as id_transaksi, COUNT(e.id_transaksi) as jumlah_orang, a.foto_bukti as gambar_paket FROM transaksi a JOIN paket b ON a.idpaket = b.id JOIN user c ON a.iduser = c.id JOIN travel d ON b.id_travel = d.id JOIN transaksi_orang e ON e.id_transaksi = a.id GROUP BY e.id_transaksi ORDER BY a.status DESC");
        return $sql;
    }
    function update_paket($id,$data){
        $this->db->where("id",$id);
        $this->db->update('transaksi',$data);
    }

    // Transaksi Muthawif
    function read_muthawif(){
        $sql = $this->db->query("SELECT *, b.nama as nama_paket, c.nama as nama_travel, COUNT(d.id_muthawif) as jumlah_muthawif, a.id as id_transaksi, a.foto_bukti as gambar_muthawif FROM request_muthawif a JOIN paket b ON a.idpaket = b.id JOIN travel c ON b.id_travel = c.id JOIN job_apply d ON a.id = d.id_request WHERE d.status_job = 'Diterima' GROUP BY d.id_muthawif");
        return $sql;
    }
    function update_muthawif($id,$data){
        $this->db->where("id",$id);
        $this->db->update('request_muthawif',$data);
    }

    // Transaksi Bursa
    function read_bursa(){
        return $this->db->query("SELECT *, b.nama as nama_paket, a.status as status_bursa, a.id as id_transaksi, a.foto_bukti as gambar_bursa FROM bursa a JOIN paket b ON a.idpaket = b.id");
    }
    function update_bursa($id,$data){
        $this->db->where("id",$id);
        $this->db->update('bursa',$data);
    }
}