<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('m_hajj');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('main');
		}        
	}

	public function index() {
		$data['title'] = 'Yaa Hajj';
		$data['sql'] = $this->m_hajj->read_admin();
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/admin',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	public function form() {
		$data['title'] = 'Yaa Hajj';
		$data['op'] = 'tambah';
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/admin',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	public function form_edit($id) {
		$data['title'] = 'Yaa Hajj';
		$data['op'] = 'edit';
		$data['sql'] = $this->m_hajj->edit_admin($id);
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/admin',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	function create() {
		$op = $this->input->post('op');
	    $id = $this->input->post('id');
    	if ($op=="tambah") {
    		$data = array(
	    		'nama' => $this->input->post('nama'),
	    		'username' => $this->input->post('username'),
	    		'password' => md5($this->input->post('password'))
	    	);
            $this->m_hajj->create_admin($data);
            $this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil disimpan !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('admin/form');
        }
        else {
        	$data = array(
	    		'nama' => $this->input->post('nama'),
	    		'username' => $this->input->post('username')
	    	);
            $this->m_hajj->update_admin($id,$data);
            $this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('admin');
        }
	}

	public function delete($id) {
		$this->m_hajj->delete_admin($id);
		$this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil dihapus !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('admin');
	}

	function cek_password(){
        $id = $this->input->post('id');
        $pass=md5($this->input->post('password_lama'));
        $cek = $this->db->query("SELECT * FROM admin where password='".$pass."'")->num_rows();
        if ($cek > 0) {
        	$data = array(
	        	'password' => md5($this->input->post('password_baru'))
	        );
	        $this->m_hajj->update_admin($id,$data);
            $this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Password berhasil diganti!</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('admin/form_edit/'.$id.'');
        }else{
            $this->session->set_flashdata('notif',
            	'<div class="alert alert-hajj alert-dismissible">
            		<strong> Password lama tidak cocok.</strong>
            			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            		<br><strong> Password gagal diganti!</strong>
            	</div>');
			redirect('admin/form_edit/'.$id.'');
        }
    }
}