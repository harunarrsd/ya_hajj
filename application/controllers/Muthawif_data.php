<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muthawif_data extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->library('upload');
		$this->load->model('m_hajj');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('main');
		}        
	}

	public function index() {
		$data['title'] = 'Yaa Hajj';
		$data['sql'] = $this->m_hajj->read_muthawif_data();
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/mutawif_data',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	public function detail($id) {
		$data['title'] = 'Yaa Hajj';
		$data['sql'] = $this->m_hajj->edit_muthawif_data($id);
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/detail_muthawif_data',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	public function form() {
		$data['title'] = 'Yaa Hajj';
		$data['op'] = 'tambah';
		$data['last'] = $this->db->query("SELECT id FROM muthawif ORDER BY id DESC LIMIT 1");
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/mutawif_data',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	public function form_edit($id) {
		$data['title'] = 'Yaa Hajj';
		$data['op'] = 'edit';
		$data['last'] = $this->db->query("SELECT id FROM muthawif ORDER BY id DESC LIMIT 1");
		$data['sql'] = $this->m_hajj->edit_muthawif_data($id);
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/mutawif_data',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	function create() {
		$op = $this->input->post('op');
	    $id = $this->input->post('id');
	    $last_id = $this->input->post('id_last');
		$filename = date("Y-m-d H:i:s")."_".$last_id;
		$filename2 = date("Y-m-d H:i:s")."_".$id;
    	if ($op=="tambah") {
    		$config = array(
				'upload_path'=>'upload/muthawif/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename
			);
			
			$this->upload->initialize($config);
			$this->upload->do_upload('gambar');
			$finfo = $this->upload->data();
			$foto_logo = $finfo['file_name'];

    		$data = array(
	    		'nama' => $this->input->post('nama'),
	    		'username' => $this->input->post('username'),
	    		'no_ktp' => $this->input->post('no_ktp'),
	    		'no_paspor' => $this->input->post('no_paspor'),
	    		'email' => $this->input->post('email'),
	    		'no_telp' => $this->input->post('no_telp'),
	    		'tgl_lahir' => $this->input->post('tgl_lahir'),
	    		'alamat' => $this->input->post('alamat'),
	    		'password' => $this->input->post('password'),
	    		'gambar' => $foto_logo
	    	);
            $this->m_hajj->create_muthawif_data($data);
            $this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil disimpan !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('muthawif_data/form');
        } else {
        	$config = array(
				'upload_path'=>'upload/muthawif/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename2
			);

			$this->upload->initialize($config);
			if($this->upload->do_upload('gambar')){
				$finfo = $this->upload->data();
				$foto_logo_edit = $finfo['file_name'];

				$data_edit = array(
		    		'nama' => $this->input->post('nama'),
		    		'username' => $this->input->post('username'),
		    		'no_ktp' => $this->input->post('no_ktp'),
		    		'no_paspor' => $this->input->post('no_paspor'),
		    		'email' => $this->input->post('email'),
		    		'no_telp' => $this->input->post('no_telp'),
		    		'tgl_lahir' => $this->input->post('tgl_lahir'),
		    		'alamat' => $this->input->post('alamat'),
		    		'gambar' => $foto_logo_edit
		    	);

				$kode_id = array('id'=>$id);
				$foto_logo_db = $this->db->get_where('muthawif',$kode_id);
				if($foto_logo_db->num_rows()>0){
					$pros=$foto_logo_db->row();
					$name_foto_logo=$pros->gambar;
	
					if(file_exists($lok=FCPATH.'upload/muthawif/'.$name_foto_logo)){
					  unlink($lok);
					}
				}
			}else{
				$data_edit = array(
		    		'nama' => $this->input->post('nama'),
		    		'username' => $this->input->post('username'),
		    		'no_ktp' => $this->input->post('no_ktp'),
		    		'no_paspor' => $this->input->post('no_paspor'),
		    		'email' => $this->input->post('email'),
		    		'no_telp' => $this->input->post('no_telp'),
		    		'tgl_lahir' => $this->input->post('tgl_lahir'),
		    		'alamat' => $this->input->post('alamat')
		    	);
			}
            $this->m_hajj->update_muthawif_data($id,$data_edit);
            $this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('muthawif_data');
        }
	}

	public function delete($id) {
		$this->m_hajj->delete_muthawif_data($id);
		$this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil dihapus !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('muthawif_data');
	}

	function reset_password($id){
		$data = array(
			'password' => 123456
		);
		$this->m_hajj->update_muthawif_data($id,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Password berhasil direset!</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('muthawif_data');
	}
}
