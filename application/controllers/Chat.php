<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('m_hajj');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('main');
		}        
	}

	public function index() {
		$data['title'] = 'Yaa Hajj';
		$data['sql'] = $this->m_hajj->read_chat();
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/chat',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	public function chat_detail($id) {
		$data['title'] = 'Yaa Hajj';
		$data['sql'] = $this->db->query("SELECT * FROM chat a JOIN user b ON a.iduser = b.id WHERE a.role = 'User' AND a.iduser = '$id'");
		$data['sql2'] = $this->db->query("SELECT * FROM chat a JOIN admin b ON a.idadmin = b.id WHERE a.role = 'Admin' AND a.iduser = '$id'");
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/chat_detail',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	function create() {
	    $id = $this->input->post('id');
		$data = array(
    		'iduser' => $this->input->post('id_user'),
    		'deskripsi' => $this->input->post('deskripsi'),
    		'idadmin' => $this->session->userdata('id'),
    		'role' => 'Admin',
    		'waktu' => date("Y-m-d H:i:s")
    	);
        $this->m_hajj->create_chat($data);
        $this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Pesan Berhasil dikirim!</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('chat');
	}
}
