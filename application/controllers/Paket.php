<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->library('upload');
		$this->load->model('m_hajj');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('main');
		}        
	}

	public function index() {
		$data['title'] = 'Yaa Hajj';
		$data['sql'] = $this->m_hajj->read_paket();
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/paket',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	function konfirmasi($id){
		$data = array(
			'status' => 4
		);
		$this->m_hajj->update_paket($id,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Transaksi berhasil!</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('paket');
	}

	function update_foto() {
	    $id = $this->input->post('id');
		$filename2 = date("Y-m-d H:i:s")."_".$id;
    	$config = array(
			'upload_path'=>'upload/paket/',
			'allowed_types'=>'jpg|png|jpeg',
			'max_size'=>2086,
			'file_name'=>$filename2
		);

		$this->upload->initialize($config);
		$this->upload->do_upload('foto_ss');
		$finfo = $this->upload->data();
		$gambar_edit = $finfo['file_name'];

		$data_edit = array(
    		'foto_ss' => $gambar_edit
    	);

		$kode_id = array('id'=>$id);
		$gambar_db = $this->db->get_where('transaksi',$kode_id);
		if($gambar_db->num_rows()>0){
			$pros=$gambar_db->row();
			$name_gambar=$pros->foto_ss;

			if(file_exists($lok=FCPATH.'upload/paket/'.$name_gambar)){
			  unlink($lok);
			}
		}
        $this->m_hajj->update_paket($id,$data_edit);
        $this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('paket');
	}
}
