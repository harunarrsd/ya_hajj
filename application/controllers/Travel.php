<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Travel extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->library('upload');
		$this->load->model('m_hajj');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('main');
		}   
	}

	public function index() {
		$data['title'] = 'Yaa Hajj';
		$data['sql'] = $this->m_hajj->read_travel();
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/travel',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	public function detail($id) {
		$data['title'] = 'Yaa Hajj';
		$data['sql'] = $this->m_hajj->edit_travel($id);
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/detail_travel',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	public function form() {
		$data['title'] = 'Yaa Hajj';
		$data['op'] = 'tambah';
		$data['last'] = $this->db->query("SELECT id FROM travel ORDER BY id DESC LIMIT 1");
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/travel',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	public function form_edit($id) {
		$data['title'] = 'Yaa Hajj';
		$data['op'] = 'edit';
		$data['last'] = $this->db->query("SELECT id FROM travel ORDER BY id DESC LIMIT 1");
		$data['sql'] = $this->m_hajj->edit_travel($id);
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/travel',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	function create() {
		$op = $this->input->post('op');
	    $id = $this->input->post('id');
	    $last_id = $this->input->post('id_last');
		$filename = date("Y-m-d H:i:s")."_".$last_id;
		$filename2 = date("Y-m-d H:i:s")."_".$id;
    	if ($op=="tambah") {
    		$config = array(
				'upload_path'=>'upload/travel/logo/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename
			);

			$config2 = array(
				'upload_path'=>'upload/travel/izin/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename
			);
			
			$this->upload->initialize($config);
			$this->upload->do_upload('foto_logo');
			$finfo = $this->upload->data();
			$foto_logo = $finfo['file_name'];

			$this->upload->initialize($config2);
			$this->upload->do_upload('foto_izin');
			$finfo2 = $this->upload->data();
			$foto_izin = $finfo2['file_name'];

    		$data = array(
	    		'nama' => $this->input->post('nama'),
	    		'deskripsi' => $this->input->post('deskripsi'),
	    		'alamat' => $this->input->post('alamat'),
	    		'nohp' => $this->input->post('nohp'),
	    		'email' => $this->input->post('email'),
	    		'no_rekening' => $this->input->post('no_rekening'),
	    		'username' => $this->input->post('username'),
	    		'password' => $this->input->post('password'),
	    		'no_izin' => $this->input->post('no_izin'),
	    		'foto_logo' => $foto_logo,
	    		'foto_izin' => $foto_izin
	    	);
            $this->m_hajj->create_travel($data);
            $this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil disimpan !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('travel/form');
        } else {
        	$config = array(
				'upload_path'=>'upload/travel/logo/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename2
			);

			$config2 = array(
				'upload_path'=>'upload/travel/izin/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename2
			);

			$this->upload->initialize($config);
			if($this->upload->do_upload('foto_logo')){
				$finfo = $this->upload->data();
				$foto_logo_edit = $finfo['file_name'];

				$this->upload->initialize($config2);
				if($this->upload->do_upload('foto_izin')){
					$finfo2 = $this->upload->data();
					$foto_izin_edit = $finfo2['file_name'];

					$data_edit = array(
			    		'nama' => $this->input->post('nama'),
			    		'deskripsi' => $this->input->post('deskripsi'),
			    		'alamat' => $this->input->post('alamat'),
			    		'nohp' => $this->input->post('nohp'),
			    		'email' => $this->input->post('email'),
			    		'no_rekening' => $this->input->post('no_rekening'),
			    		'username' => $this->input->post('username'),
			    		'no_izin' => $this->input->post('no_izin'),
			    		'foto_logo' => $foto_logo_edit,
			    		'foto_izin' => $foto_izin_edit
			    	);
				}

				$kode_id = array('id'=>$id);
				$foto_logo_db = $this->db->get_where('travel',$kode_id);
				$foto_izin_db = $this->db->get_where('travel',$kode_id);
				if($foto_logo_db->num_rows()>0){
					$pros=$foto_logo_db->row();
					$name_foto_logo=$pros->foto_logo;
	
					if(file_exists($lok=FCPATH.'upload/travel/logo/'.$name_foto_logo)){
					  unlink($lok);
					}
				}
				if($foto_izin_db->num_rows()>0){
					$pros=$foto_izin_db->row();
					$name_foto_izin=$pros->foto_izin;
	
					if(file_exists($lok=FCPATH.'upload/travel/izin/'.$name_foto_izin)){
					  unlink($lok);
					}
				}
			}else{
				$data_edit = array(
		    		'nama' => $this->input->post('nama'),
		    		'deskripsi' => $this->input->post('deskripsi'),
		    		'alamat' => $this->input->post('alamat'),
		    		'nohp' => $this->input->post('nohp'),
		    		'email' => $this->input->post('email'),
		    		'no_rekening' => $this->input->post('no_rekening'),
		    		'username' => $this->input->post('username'),
		    		'no_izin' => $this->input->post('no_izin')
		    	);
			}
            $this->m_hajj->update_travel($id,$data_edit);
            $this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('travel');
        }
	}

	public function delete($id) {
		$this->m_hajj->delete_travel($id);
		$this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil dihapus !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('travel');
	}

	function reset_password($id){
		$data = array(
			'password' => 123456
		);
		$this->m_hajj->update_travel($id,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Password berhasil direset!</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('travel');
	}
}
