<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->library('upload');
        $this->load->helper('text');
		$this->load->model('m_hajj');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('main');
		}        
	}

	public function index() {
		$data['title'] = 'Yaa Hajj';
		$data['sql'] = $this->m_hajj->read_blog();
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/blog',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	public function form() {
		$data['title'] = 'Yaa Hajj';
		$data['op'] = 'tambah';
		$data['last'] = $this->db->query("SELECT id FROM blog ORDER BY id DESC LIMIT 1");
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/blog',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	public function form_edit($id) {
		$data['title'] = 'Yaa Hajj';
		$data['op'] = 'edit';
		$data['last'] = $this->db->query("SELECT id FROM blog ORDER BY id DESC LIMIT 1");
		$data['sql'] = $this->m_hajj->edit_blog($id);
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/blog',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	function create() {
		$op = $this->input->post('op');
	    $id = $this->input->post('id');
	    $last_id = $this->input->post('id_last');
		$filename = date("Y-m-d H:i:s")."_".$last_id;
		$filename2 = date("Y-m-d H:i:s")."_".$id;
    	if ($op=="tambah") {
    		$config = array(
				'upload_path'=>'upload/blog/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename
			);
			
			$this->upload->initialize($config);
			$this->upload->do_upload('gambar');
			$finfo = $this->upload->data();
			$gambar = $finfo['file_name'];

    		$data = array(
	    		'judul' => $this->input->post('judul'),
	    		'deskripsi' => $this->input->post('deskripsi'),
	    		'gambar' => $gambar,
	    		'idadmin' => $this->session->userdata('id'),
	    		'tanggal' => date("Y-m-d H:i:s")
	    	);
            $this->m_hajj->create_blog($data);
            $this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil disimpan !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('blog/form');
        } else {
        	$config = array(
				'upload_path'=>'upload/blog/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename2
			);

			$this->upload->initialize($config);
			if($this->upload->do_upload('gambar')){
				$finfo = $this->upload->data();
				$gambar_edit = $finfo['file_name'];

				$data_edit = array(
		    		'judul' => $this->input->post('judul'),
		    		'deskripsi' => $this->input->post('deskripsi'),
		    		'gambar' => $gambar_edit,
		    		'idadmin' => $this->session->userdata('id'),
		    		'tanggal' => date("Y-m-d H:i:s")
		    	);

				$kode_id = array('id'=>$id);
				$gambar_db = $this->db->get_where('blog',$kode_id);
				if($gambar_db->num_rows()>0){
					$pros=$gambar_db->row();
					$name_gambar=$pros->gambar;
	
					if(file_exists($lok=FCPATH.'upload/blog/'.$name_gambar)){
					  unlink($lok);
					}
				}
			}else{
				$data_edit = array(
		    		'judul' => $this->input->post('judul'),
		    		'deskripsi' => $this->input->post('deskripsi'),
		    		'idadmin' => $this->session->userdata('id'),
		    		'tanggal' => date("Y-m-d H:i:s")
		    	);
			}
            $this->m_hajj->update_blog($id,$data_edit);
            $this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('blog');
        }
	}

	public function delete($id) {
		$this->m_hajj->delete_blog($id);
		$this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Data berhasil dihapus !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('blog');
	}
}
