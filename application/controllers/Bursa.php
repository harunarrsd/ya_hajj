<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bursa extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('m_hajj');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('main');
		}        
	}

	public function index() {
		$data['title'] = 'Yaa Hajj';
		$data['sql'] = $this->m_hajj->read_bursa();
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/bursa',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}

	function konfirmasi($id){
		$data = array(
			'status' => 3
		);
		$this->m_hajj->update_bursa($id,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Transaksi berhasil!</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('bursa');
	}
}
