<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('m_hajj');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('main');
		}        
	}

	public function index() {
		$data['title'] = 'Yaa Hajj';
		$data['user'] = $this->m_hajj->read_user();
		$data['travel'] = $this->m_hajj->read_travel();
		$data['paket'] = $this->m_hajj->read_paket();
		$data['muthawif'] = $this->m_hajj->read_muthawif();
		$data['banner'] = $this->m_hajj->read_banner();
		$data['blog'] = $this->m_hajj->read_blog();
		$data['bursa'] = $this->m_hajj->read_bursa();
		$data['admin'] = $this->m_hajj->read_admin();
		$data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/dashboard',array('main'=>$data),true);
		$this->load->view('main',array('main'=>$data));
	}
}
