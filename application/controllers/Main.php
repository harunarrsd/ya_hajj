<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('m_hajj');     
	}

	public function index()	{
		$data['title'] = 'Yaa Hajj';
        $data['pages'] = $this->load->view('pages/login','',true);
		$this->load->view('main_login',array('main'=>$data));
	}

	// login
	function ceklogin(){
		if (isset($_POST['login'])) {
			$user=$this->input->post('username',true);
			$pass=md5($this->input->post('password',true));
			$cek=$this->m_hajj->proseslogin($user, $pass);
			$hasil=count($cek);
			if ($hasil > 0) {
				$yglogin=$this->db->get_where('admin',array('username'=>$user, 'password'=>$pass))->row();
				$data = array('udhmasuk' => true,
				'id'=>$yglogin->id,
				'nama' => $yglogin->nama);
				$this->session->set_userdata($data);
				redirect('dashboard');
			}else {
				$this->session->set_flashdata('notif','<div class="alert alert-hajj alert-dismissible"><strong> Maaf Email atau Password Anda Salah ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
				redirect('main');
			}
		}
	}

	// logout
	function keluar(){
		$this->session->sess_destroy();
		redirect('main');
	}
}
